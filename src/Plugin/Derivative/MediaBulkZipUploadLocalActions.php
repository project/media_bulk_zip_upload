<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Media bulk zip upload actions for entities.
 */
class MediaBulkZipUploadLocalActions extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Creates a new MediaBulkZipUploadLocalActions.
   *
   * @param string $basePluginId
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    protected string $basePluginId,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $enabledTypes = $this->configFactory->get('media_bulk_zip_upload.settings')->get('media_types') ?? [];

    $mediaTypes = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    foreach ($mediaTypes as $mediaType) {
      \assert($mediaType instanceof MediaTypeInterface);
      if (!\in_array($mediaType->id(), $enabledTypes, TRUE)) {
        continue;
      }

      $this->derivatives["media_bulk_zip_upload.{$mediaType->id()}"] = [
        'route_name' => 'media_bulk_zip_upload.form',
        'route_parameters' => ['media_type' => $mediaType->id()],
        'appears_on' => ['entity.media.collection'],
        'title' => $this->t('Bulk upload @type media', [
          '@type' => $mediaType->label(),
        ]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
