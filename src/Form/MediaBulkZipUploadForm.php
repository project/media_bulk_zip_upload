<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_bulk_zip_upload\Event\MediaBulkZipUploadPreSaveEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Defines a class for a form for bulk uploading media.
 */
class MediaBulkZipUploadForm extends FormBase {

  /**
   * The base media entity used to clone.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected MediaInterface $entity;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The uploaded zip file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected FileInterface $zipFile;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The media entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $mediaStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->fileSystem = $container->get('file_system');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->mediaStorage = $container->get('entity_type.manager')->getStorage('media');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'media_bulk_zip_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?MediaTypeInterface $media_type = NULL): array {
    $form['zip'] = [
      '#type' => 'file',
      '#weight' => -100,
      '#title' => $this->t('Upload Zip File'),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#upload_validators' => ['file_validate_size' => [Environment::getUploadMaxSize()]],
      ],
    ];

    $this->entity = $this->mediaStorage->create([
      'bundle' => $media_type->id(),
    ]);

    // Apply the bulk_upload form mode to this form.
    $form_display = EntityFormDisplay::collectRenderDisplay($this->entity, 'media_bulk_zip_upload');
    $form_display->buildForm($this->entity, $form, $form_state);
    $form_state->set('form_display', $form_display);

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 1000,
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Upload'),
        '#button_type' => 'primary',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!$result = \file_save_upload('zip', [
      'FileExtension' => ['extensions' => 'zip'],
      'FileSizeLimit' => ['fileLimit' => Environment::getUploadMaxSize()],
    ], NULL, 0)) {
      $form_state->setErrorByName('zip', $this->t('Failed to upload the file.'));
      return;
    }
    \assert($result instanceof FileInterface);
    $this->zipFile = $result;

    $form_display = EntityFormDisplay::collectRenderDisplay($this->entity, 'media_bulk_zip_upload');
    $form_display->extractFormValues($this->entity, $form, $form_state);
    // Invoke all specified builders for copying form values to entity fields.
    if (isset($form['#entity_builders'])) {
      foreach ($form['#entity_builders'] as $function) {
        \call_user_func_array($function, [
          'media',
          $this->entity,
          &$form,
          &$form_state,
        ]);
      }
    }

    $violations = $this->entity->validate();

    // Remove violations of inaccessible fields and not edited fields.
    $violations
      ->filterByFieldAccess($this->currentUser())
      ->filterByFields(\array_diff(\array_keys($this->entity->getFieldDefinitions()), \array_keys($form_display->getComponents())));

    // Flag entity level violations.
    foreach ($violations->getEntityViolations() as $violation) {
      /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
      $form_state->setErrorByName('', $violation->getMessage());
    }
    // Let the form display flag violations of its fields.
    $form_display->flagWidgetsErrorsFromViolations($violations, $form, $form_state);

    // The entity was validated.
    $this->entity->setValidationRequired(FALSE);
    $form_state->setTemporaryValue('entity_validated', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $zip = new \ZipArchive();

    $batch = [
      'init_message' => $this->t('Starting bulk upload'),
      'progress_message' => $this->t('Running the bulk upload'),
      'error_message' => $this->t('An error occurred while uploading'),
      'title' => $this->t('Running the bulk upload'),
      'operations' => [],
      'finished' => [static::class, 'done'],
    ];
    $path = $this->fileSystem->realpath($this->zipFile->getFileUri());
    if ($zip->open($path)) {
      $mediaType = $this->entity->bundle->entity;
      $sourceField = $mediaType->getSource()->getSourceFieldDefinition($mediaType);
      $allowed_extensions = \explode(' ', $sourceField->getSetting('file_extensions'));
      for ($i = 0; $i < $zip->numFiles; $i++) {
        $filename = (string) $zip->getNameIndex($i);
        $basename = \basename($filename);
        if (\str_ends_with($filename, '/') || $filename[0] === '.' || $filename[0] === '_' || $basename[0] === '_' || $basename[0] === '.') {
          // Skip directories . files and OSX cruft.
          continue;
        }

        $fileInfo = \pathinfo($filename);
        if (empty($fileInfo['extension']) || !\in_array($fileInfo['extension'], $allowed_extensions, TRUE)) {
          continue;
        }
        $batch['operations'][] = [
          [static::class, 'processOneFile'],
          [
            'zip://' . $path . "#" . $filename,
            $this->entity,
          ],
        ];
      }
      $zip->close();
    }
    \batch_set($batch);
    $form_state->setRedirect('entity.media.collection');
  }

  /**
   * Batch callback.
   *
   * @param string $zip_stream_reference
   *   File name.
   * @param \Drupal\media\MediaInterface $entity
   *   Reference entity.
   * @param array $context
   *   Context.
   */
  public static function processOneFile(string $zip_stream_reference, MediaInterface $entity, array &$context): void {
    $media = $entity->createDuplicate();
    $mediaType = $media->bundle->entity;
    $sourceField = $mediaType->getSource()->getSourceFieldDefinition($mediaType);

    $basename = \basename($zip_stream_reference);
    $filename = $basename;
    if (\str_contains($basename, '#')) {
      // Root level file in the zip.
      [, $filename] = \explode('#', $basename);
    }

    $fieldStorageDefinition = $sourceField->getFieldStorageDefinition();
    $directory = $fieldStorageDefinition->getSetting('uri_scheme') . '://' . \Drupal::token()->replace($sourceField->getSetting('file_directory'));
    $destination = $directory . '/' . $filename;
    $fileSystem = \Drupal::service('file_system');
    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    if ($fileSystem->saveData(\file_get_contents($zip_stream_reference), $destination)) {
      $realpath = $fileSystem->realpath($destination);
      $file = File::create([
        'uid' => \Drupal::currentUser()->id(),
        'status' => 1,
        'uri' => $destination,
        'filesize' => \filesize($realpath),
      ]);
      $media->set($sourceField->getName(), $file);
      $media->setName(\substr($file->getFilename(), 0, 250));

      $violations = $media->validate();
      if (\count($violations)) {
        \Drupal::service('messenger')
          ->addError(new TranslatableMarkup('Could not create media for %file: @messages', [
            '%file' => $filename,
            '@messages' => \array_map(static function (ConstraintViolation $violation) {
              return $violation->getMessage();
            }, \iterator_to_array($violations)),
          ]));
        return;
      }

      // Dispatch a pre-save event to allow subscribers to alter media before
      // saving.
      $event = new MediaBulkZipUploadPreSaveEvent($media);
      \Drupal::service('event_dispatcher')->dispatch($event);
      $media = $event->getMedia();

      $media->save();
      $context['results'][] = $media->id();
      return;
    }
    \Drupal::service('messenger')->addError(new TranslatableMarkup('Could not extract file %file', ['%file' => $filename]));
  }

  /**
   * Finished callback.
   *
   * @param bool $success
   *   Status.
   * @param array $results
   *   Results.
   * @param array $operations
   *   Operations.
   */
  public static function done($success, array $results, array $operations): void {
    $messenger = \Drupal::service('messenger');
    if ($success) {
      $messenger->addStatus(new PluralTranslatableMarkup(\count($results), 'Imported one file as media', 'Imported @count files as media'));
      return;
    }
    $messenger->addError('An error occurred during import, please review the messages.');
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title(MediaTypeInterface $media_type): TranslatableMarkup {
    return $this->t('Bulk upload @type media', ['@type' => $media_type->label()]);
  }

  /**
   * Checks if the user has access to the bulk upload form.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(AccountInterface $account, MediaTypeInterface $media_type): AccessResult {
    $config = $this->config(MediaBulkZipUploadSettingsForm::CONFIG_NAME);
    $enabledTypes = $config->get('media_types') ?? [];

    if (!\in_array($media_type->id(), $enabledTypes, TRUE)) {
      return AccessResult::forbidden(\sprintf('Media type %s is not enabled for bulk upload.', $media_type->label()))->addCacheableDependency($config);
    }

    // User must have this module's permission as well as access to create this
    // media type.
    return AccessResult::allowedIfHasPermission($account, "use media bulk zip upload for {$media_type->id()}")->andIf(
      $this->entityTypeManager->getAccessControlHandler('media')->createAccess($media_type->id(), $account, [], TRUE),
    )->addCacheableDependency($config);
  }

}
