<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Media Bulk Zip Upload settings.
 */
class MediaBulkZipUploadSettingsForm extends ConfigFormBase {

  public const CONFIG_NAME = 'media_bulk_zip_upload.settings';

  /**
   * Constructs a new MediaBulkZipUploadSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_bulk_zip_upload_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);
    $mediaTypes = $this->entity_type_manager->getStorage('media_type')->loadMultiple();
    $form['media_types'] = [
      '#title' => $this->t('Media types'),
      '#description' => $this->t('Allow these media types to be bulk uploaded with zip files'),
      '#type' => 'checkboxes',
      '#default_value' => $config->get('media_types') ?? [],
      '#options' => \array_reduce($mediaTypes, static function (array $carry, MediaTypeInterface $mediaType) {
        $carry[$mediaType->id()] = $mediaType->label();
        return $carry;
      }, []),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config(self::CONFIG_NAME);
    $config->set('media_types', $form_state->getValue('media_types'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
