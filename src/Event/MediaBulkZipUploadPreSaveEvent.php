<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\media\MediaInterface;

/**
 * An event during bulk upload that lets subscribers alter media before saving.
 */
class MediaBulkZipUploadPreSaveEvent extends Event {

  /**
   * Constructs a new MediaBulkZipUploadPreSaveEvent.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   */
  public function __construct(protected MediaInterface $media) {
  }

  /**
   * Gets the value of Media.
   *
   * @return \Drupal\media\MediaInterface
   *   Value of Media.
   */
  public function getMedia(): MediaInterface {
    return $this->media;
  }

  /**
   * Sets the value of Media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The value for Media.
   *
   * @return $this
   *   The calling class.
   */
  public function setMedia(MediaInterface $media): static {
    $this->media = $media;
    return $this;
  }

}
