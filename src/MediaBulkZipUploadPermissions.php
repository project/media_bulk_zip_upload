<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\MediaType;

/**
 * Provides dynamic permissions.
 */
class MediaBulkZipUploadPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of permissions.
   *
   * @return array
   *   The permissions.
   */
  public function permissions(): array {
    $permissions = [];
    foreach (MediaType::loadMultiple() as $media_type) {
      $permissions += $this->buildPermissions($media_type);
    }
    return $permissions;
  }

  /**
   * Returns a list of permissions for a media type.
   *
   * @param \Drupal\media\Entity\MediaType $media_type
   *   The media type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(MediaType $media_type): array {
    return [
      "use media bulk zip upload for {$media_type->id()}" => [
        'title' => $this->t('%type_name: Use media bulk zip upload form', ['%type_name' => $media_type->label()]),
      ],
    ];
  }

}
