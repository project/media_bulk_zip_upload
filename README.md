INTRODUCTION
---------------

Provides the ability to create media entities from a Zip file.

USAGE
--------

Go to /admin/config/media/media-bulk-zip-upload-config and enable buk uploading on some Media types.
Go to /admin/content/media and use the action links to visit the bulk upload form
Configure the Media Bulk Zip Upload form mode to change which fields are displayed on this form.
