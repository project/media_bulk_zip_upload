<?php

declare(strict_types=1);

namespace Drupal\Tests\media_bulk_zip_upload\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Defines a class for testing bulk upload.
 *
 * @group media_bulk_zip_upload
 */
class MediaBulkZipUploadFormTest extends BrowserTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'media',
    'media_bulk_zip_upload',
    'media_bulk_zip_upload_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_actions_block');
    $this->createMediaType('file', [
      'id' => 'type_one',
      'label' => 'Type one',
    ]);
    $this->createMediaType('file', [
      'id' => 'type_two',
      'label' => 'Type two',
    ]);
  }

  /**
   * Tests bulk upload.
   */
  public function testBulkZipUpload(): void {
    // Ensure our custom form display modes exist.
    media_bulk_zip_upload_install(FALSE);
    $this->assertNotNull(EntityFormDisplay::load('media.type_one.media_bulk_zip_upload'));
    $this->assertNotNull(EntityFormDisplay::load('media.type_two.media_bulk_zip_upload'));

    $assert = $this->assertSession();
    $settingsUrl = Url::fromRoute('media_bulk_zip_upload.settings');

    $this->drupalGet($settingsUrl);
    $assert->statusCodeEquals(403);

    $adminUser = $this->drupalCreateUser([
      'administer media',
    ]);
    $this->drupalLogin($adminUser);

    $this->drupalGet($settingsUrl);
    $assert->statusCodeEquals(200);

    $this->submitForm([
      'media_types[type_one]' => 1,
      'media_types[type_two]' => 1,
    ], 'Save configuration');
    $assert->pageTextContains('The configuration options have been saved.');

    $mediaCreateUser = $this->drupalCreateUser([
      // @todo import media view so we can remove this permission.
      'administer media',
      'access media overview',
      'use media bulk zip upload for type_one',
      'create type_one media',
      'view media',
    ]);
    $this->drupalLogin($mediaCreateUser);

    $this->drupalGet(Url::fromRoute('entity.media.collection'));
    $assert->statusCodeEquals(200);
    // @todo figure out why these aren't showing in tests.
    // $assert->linkExists('Bulk upload Type one media');
    $assert->linkNotExists('Bulk upload Type two media');

    $this->drupalGet('/media/add/type_two/bulk');
    $assert->statusCodeEquals(403);

    $this->drupalGet('/media/add/type_one/bulk');
    $assert->statusCodeEquals(200);

    // @see _media_bulk_zip_upload_configure_form_display
    $assert->fieldNotExists('files[field_media_file_0]');
    $assert->fieldNotExists('name[0][value]');

    $file = \Drupal::service('extension.path.resolver')->getPath('module', 'media_bulk_zip_upload') . '/tests/fixtures/Archive.zip';
    $media_storage = \Drupal::entityTypeManager()->getStorage('media');

    $this->submitForm([
      'files[zip]' => $file,
      'revision_log_message[0][value]' => 'Bulk uploaded!',
    ], 'Upload');
    $media = $media_storage->getQuery()->accessCheck(TRUE)->execute();
    $this->assertCount(3, $media);

    $entities = $media_storage->loadMultiple($media);
    $docx = \reset($entities);
    $pdf2 = \next($entities);
    $pdf = \end($entities);
    $this->assertEquals('dummy.pdf', $pdf->label());
    $this->assertEquals('dummy.pdf', $pdf->field_media_file->entity->getFilename());
    $this->assertEquals('Bulk uploaded!', $pdf->getRevisionLogMessage());
    $this->assertFalse($pdf->isPublished());
    $this->assertEquals('dummy2.pdf', $pdf2->label());
    $this->assertEquals('dummy2.pdf', $pdf2->field_media_file->entity->getFilename());
    $this->assertEquals('Bulk uploaded!', $pdf2->getRevisionLogMessage());
    $this->assertFalse($pdf2->isPublished());
    $this->assertEquals('test.docx', $docx->label());
    $this->assertEquals('Bulk uploaded!', $docx->getRevisionLogMessage());
    $this->assertFalse($docx->isPublished());

    /** @var \Drupal\file\FileInterface $file_entity */
    $file_entity = $docx->field_media_file->entity;
    $this->assertEquals('test.docx', $file_entity->getFilename());
    $this->assertStringContainsString(\date('Y-m'), $file_entity->getFileUri());
    $this->drupalGet($docx->toUrl('edit-form'));
    $assert->statusCodeEquals(200);
  }

  /**
   * Tests creating a new media type via the UI adds the form mode.
   */
  public function testMediaTypeCreate(): void {
    $this->drupalLogin($this->drupalCreateUser([
      'access media overview',
      'administer media',
      'administer media types',
      'view media',
    ]));

    $label = $this->randomMachineName();
    $this->drupalGet('admin/structure/media/add');
    $this->submitForm([
      'label' => $label,
      'id' => 'type_three',
      'source' => 'file',
    ], 'Save');
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains("A Media bulk zip upload form display has been created for the $label media type.");
    $this->assertNotNull(EntityFormDisplay::load('media.type_three.media_bulk_zip_upload'));
  }

}
