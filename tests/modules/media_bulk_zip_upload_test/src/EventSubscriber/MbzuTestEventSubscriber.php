<?php

declare(strict_types=1);

namespace Drupal\media_bulk_zip_upload_test\EventSubscriber;

use Drupal\media_bulk_zip_upload\Event\MediaBulkZipUploadPreSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Test subscriber for media_bulk_zip_upload pre save events.
 */
class MbzuTestEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MediaBulkZipUploadPreSaveEvent::class][] = ['modifyMedia', 100];
    return $events;
  }

  /**
   * Modifies media before saving it.
   *
   * @param \Drupal\media_bulk_zip_upload\Event\MediaBulkZipUploadPreSaveEvent $event
   *   The event.
   */
  public function modifyMedia(MediaBulkZipUploadPreSaveEvent $event): void {
    $media = $event->getMedia();
    $media->setUnpublished();
    $event->setMedia($media);
  }

}
